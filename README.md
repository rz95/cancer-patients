# Cancer Patients

Many people's lives are cut short due to cancer. However, due to the age of big data, we are able to combat this malicious disease.

This code achieves 99% accuracy in using SVM to model the patients' level of cancer based on their lifestyles. The dataset comes from Kaggle: https://www.kaggle.com/rishidamarla/cancer-patients-data.
